# Summary #
Learning resource sharing board

## FYI, how to edit readme.md ##
Markdown syntax
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lines

# Nov 2, 2018 #
## How to connect to Power 8 ##
COH, laptop, use wifi: WUSM-secure, login: wudosis\username, your wudosis password

## Tutorial for tensorflow ##
https://github.com/aymericdamien/TensorFlow-Examples

## Python IDE ##
	anaconda->spider
	sublime text
	atom
    
## Learning Path/Workflow ##
1.	install anaconda (add to PATH , powershell .sh)

2.	create pyhon=x.x ‘tensorflow=x.x’ evnvironment

3.	test the examples
	* Prerequisite: general reading; basic concept
	* Introduction: hello world, basic operations; basic knowledge of py
	* Basic models: linear regression; use tf in a statistics problem
	* Neural networks: simple neural network (NN), CNN; basic machine learning(ML)	
	* GAN: unsupervised
	
4.	Test your codes on local

5.	Try ‘tensorflow-gpu’, computation using GPU (computation)
	* Nvida-driver
	* CUDA toolkit 9.0
	* cuDNN 7

6.	Put to Power 8
	* TO-BE-ADDED



# XXX XX, 2018 #


# Who do I talk to? #
Zach